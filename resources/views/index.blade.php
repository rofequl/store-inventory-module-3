@extends('layout.app')
@section('title','Army Golf Club | Store Room Management')
@section('content')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Store Inventory</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 mb-4" style="cursor: pointer"
                 onclick="window.location.href='{{route('inventory.index')}}'">
                <div class="stats-small stats-small--1 card card-small" style="min-height: 5.7rem!important;">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <h6 class="stats-small__value my-3"><i class="material-icons">autorenew</i>
                                </h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="stats-small__label text-uppercase">STOCK ENTRY</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mb-4" style="cursor: pointer"
                 onclick="window.location.href='{{route('stock.index')}}'">
                <div class="stats-small stats-small--1 card card-small" style="min-height: 5.7rem!important;">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <h6 class="stats-small__value my-3"><i class="material-icons">assignment_late</i>
                                </h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="stats-small__label text-uppercase">STOCK DELIVERY</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mb-4" style="cursor: pointer"
                 onclick="window.location.href='{{route('temp.index')}}'">
                <div class="stats-small stats-small--1 card card-small" style="min-height: 5.7rem!important;">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <h6 class="stats-small__value my-3"><i class="material-icons">schedule</i>
                                </h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="stats-small__label text-uppercase">DAILY TEMP. OUT / IN</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mb-4" style="cursor: pointer"
                 onclick="window.location.href='{{route('conditioning.index')}}'">
                <div class="stats-small stats-small--1 card card-small" style="min-height: 5.7rem!important;">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <h6 class="stats-small__value my-3"><i class="material-icons">assignment_turned_in</i>
                                </h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="stats-small__label text-uppercase">STOCK CONDITIONING</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('style')

@endpush
@push('script')
    <script src="{{asset('assets/scripts/app/app-ecommerce.1.3.1.min.js')}}"></script>
@endpush


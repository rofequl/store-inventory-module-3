<div class="main-navbar sticky-top bg-white">
    <!-- Main Navbar -->
    <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
        <ul class="nav main-navbar__search w-100 d-none d-md-flex d-lg-flex py-2">
            <li class="nav-item">
                <a class="nav-link" style="color: #3d5170" href="{{route('supplier.index')}}">Supplier</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: #3d5170" href="{{route('source.index')}}">Source</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: #3d5170" href="{{route('department.index')}}">Department</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: #3d5170" href="{{route('product.index')}}">Product Entry</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: #3d5170" href="{{route('condition.index')}}">Working Conditioning</a>
            </li>
        </ul>
        <ul class="navbar-nav border-left flex-row ">

        </ul>
        <nav class="nav">
            <a href="#"
               class="nav-link nav-link-icon toggle-sidebar d-sm-inline d-md-none text-center border-left"
               data-toggle="collapse" data-target=".header-navbar" aria-expanded="false"
               aria-controls="header-navbar">
                <i class="material-icons">&#xE5D2;</i>
            </a>
        </nav>
    </nav>
</div> <!-- / .main-navbar -->

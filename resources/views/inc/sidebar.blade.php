<!-- Main Sidebar -->
<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0" style="z-index: 1040">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="{{route('store')}}" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;"
                         src="{{asset('assets/images/logo.jpg')}}" alt="Store Inventory">
                    <span class="d-none d-md-inline ml-1">Store Inventory</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
        <div class="input-group input-group-seamless ml-3">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
            <input class="navbar-search form-control" type="text" placeholder="Search for something..."
                   aria-label="Search">
        </div>
    </form>
    <div class="nav-wrapper">
        <ul class="nav nav--no-borders flex-column">
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'store'?'active':''}}" href="{{route('store')}}">
                    <i class="material-icons">dashboard</i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'inventory.index'?'active':''}}"
                   href="{{route('inventory.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Stock Entry</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'stock.index'?'active':''}}"
                   href="{{route('stock.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Stock Delivery</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'temp.index'?'active':''}}"
                   href="{{route('temp.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Daily Temp. Out / In</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'conditioning.index'?'active':''}}"
                   href="{{route('conditioning.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Stock Conditioning</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'stock.report'?'active':''}}"
                   href="{{route('stock.report')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Stock Report</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'department.report'?'active':''}}"
                   href="{{route('department.report')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Department Stock Report</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'temp.report'?'active':''}}"
                   href="{{route('temp.report')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Daily Temp. Out / In Ledger</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'store.report'?'active':''}}"
                   href="{{route('store.report')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Stock Conditioning Report</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{Route::current()->getName() == 'supplier.report'?'active':''}}"
                   href="{{route('supplier.report')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Supplier Report</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<!-- End Main Sidebar -->

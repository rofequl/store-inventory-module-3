@extends('layout.app')
@section('title','Army Golf Club | Supplier Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times-circle mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Stock Manage</span>
                <h3 class="page-title">Daily Temp. Out / In</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a id="add-new-event" role="button" href="#" class="btn btn-primary" data-toggle="modal"
                       data-target="#exampleModal">
                        <i class="material-icons">add</i> New Temp. In </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <table class="transaction-history d-none">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Department</th>
                <th>Receiver Name</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Qty</th>
                <th>Out</th>
                <th>In</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>@php $sl = 1; @endphp
            @foreach($product as $products)
                <tr>
                    <td>{{$sl}}</td>@php $sl++; @endphp
                    <td>{{date('M dS Y', strtotime($products->date))}}</td>
                    <td>{{$products->department->department_name}}</td>
                    <td>{{$products->receiver_name}}</td>
                    <td>{{$products->product->product_name}}</td>
                    <td>{{$products->product->product_code}}</td>
                    <td>{{$products->qty}}</td>
                    <td>{{$products->out}}</td>
                    <td>{{$products->in}}</td>
                    <td>
                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                            <button type="button" class="btn btn-white view" id="{{$products->id}}">
                                Temp. Out
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- End Transaction History Table -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('temp.store')}}" autocomplete="off">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Insert Daily Temp. In</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-row">
                            <div class="input-daterange input-group input-group-sm ml-auto col-md-6">
                                <input type="text" class="input-sm form-control datepicker" name="date"
                                       placeholder="Date" id="analytics-overview-date-range-1">
                                <span class="input-group-append">
                                    <span class="input-group-text">
                                      <i class="material-icons">&#xE916;</i>
                                    </span>
                                  </span>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control product" name="product_code"
                                       placeholder="Enter Product Code">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control product_name"
                                       placeholder="Product Name" readonly>
                                <input type="hidden" class="product_id" name="product_id" value="">
                            </div>
                            <div class="form-group col-md-6">
                                <select name="department_id" class="form-control department" required>
                                    <option selected disabled>Select Departments</option>
                                    @foreach($department as $departments)
                                        <option value="{{$departments->id}}">{{$departments->department_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="receiver_name"
                                       placeholder="Enter Receiver Name" required>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" class="form-control product-qty-input" name="qty"
                                       placeholder="Enter Product Qty" readonly required>
                                <div class="invalid-feedback product-qty">ok</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabe2"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('temp.out')}}" autocomplete="off">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Insert Daily Temp. Out</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="number" class="form-control product-qty-input" name="qty"
                                       placeholder="Enter Product Qty" required>
                                <input type="hidden" class="product-id-input" name="id" required>
                                <div class="invalid-feedback product-qty">ok</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script>

        $('.datepicker').datepicker("setDate", new Date());

        $(document).ready(function () {
            $(document).on('keyup', '.product', function () {
                let code = $(this).val();
                $.ajax({
                    url: "{!! route('product.show','') !!}" + "/" + code,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            $('.product_name').val(data.product_name);
                            $('.product_id').val(data.id);
                            $('.purchase_unit_price').val(data.product_unit_price);
                            quantity();
                        } else {
                            $('.product_name').val('');
                            $('.product_id').val('');
                            $('.purchase_unit_price').val('');
                        }
                    }
                });
            });
        });

        function quantity() {
            let product = $('.product_id').val(), id = $('.product').val();
            if (product != "") {
                $.ajax({
                    url: "{{route('inventory.qty')}}",
                    type: 'get',
                    data: {product: id},
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            $('.product-qty').show();
                            $('.product-qty').html(data + ' Product available');
                            $('.product-qty-input').prop('readonly', false);
                        } else {
                            $('.product-qty').show();
                            $('.product-qty').html('No product found');
                            $('.product-qty-input').prop('readonly', true).val('');
                        }
                    }
                });
            }
        }

        $('.view').click(function () {
            let id = $(this).attr("id");
            $('.product-id-input').val(id);
            $('#exampleModal2').modal('show');
        });
    </script>
@endpush

@extends('layout.app')
@section('title','Army Golf Club | Supplier Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times-circle mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Product Management</span>
                <h3 class="page-title">Stock Entry</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <div class="col mb-5">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom text-right">
                    <span onclick="window.location.href='{{route('inventory.index')}}'" class="mb-0"
                          style="cursor: pointer"><i
                            class="fas fa-hand-point-left"></i> Go back </span>
                </div>
                <div class="card-body p-0 text-center">
                    <form method="post" action="{{route('inventory.store')}}" id="upload_form"
                          enctype="multipart/form-data" autocomplete="off">
                        {{csrf_field()}}
                        <div class="row p-3">
                            <div class="col-7">
                                <div class="row">
                                    <div class="col-2 text-right"></div>
                                    <div class="form-group col-md-8">
                                        <div class="form-check custom-radio form-check-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="member_edit" id="inlineRadio1" value="0" checked>
                                            <label class="custom-control-label" for="inlineRadio1">Supplier</label>
                                        </div>
                                        <div class="form-check custom-radio form-check-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="member_edit" id="inlineRadio2" value="1">
                                            <label class="custom-control-label" for="inlineRadio2">Source</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="member_id">
                                    <div class="col-4 text-right">Supplier</div>
                                    <div class="form-group col-md-4">
                                        <select name="supplier_id" class="form-control" required>
                                            <option selected disabled>Select Supplier</option>
                                            @foreach($supplier as $suppliers)
                                                <option value="{{$suppliers->id}}">{{$suppliers->owner_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row" id="member_id2" style="display: none">
                                    <div class="col-4 text-right">Source</div>
                                    <div class="form-group col-md-4">
                                        <select name="source_id" class="form-control" required>
                                            <option selected disabled>Select Source</option>
                                            @foreach($source as $sources)
                                                <option value="{{$sources->id}}">{{$sources->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="row">
                                    <div class="col-6 text-right">Voucher No.</div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" name="purchase_code"
                                               value="{{$purchase_no}}" required>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 text-right">Current Date</div>
                                    <div class="input-daterange input-group input-group-sm ml-auto col-md-6">
                                        <input type="text" class="input-sm form-control datepicker" name="date"
                                               placeholder="Date" id="analytics-overview-date-range-1" required>
                                        <span class="input-group-append">
                                            <span class="input-group-text">
                                              <i class="material-icons">&#xE916;</i>
                                            </span>
                                          </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table mb-0 mt-4">
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Product Code</th>
                                <th scope="col" class="border-0">Product Name</th>
                                <th scope="col" class="border-0">Price</th>
                                <th scope="col" class="border-0">Quantity</th>
                                <th scope="col" class="border-0">Total Amount</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody class="AddPurchaseDiv">
                            <tr>
                                <td class="Sl">1</td>
                                <td><input id="SelectProduct1" type="text" name="product_code[]"
                                           class="form-control SelectProduct" required></td>
                                <td><input name="product_name[]" id="product_name1" type="text"
                                           class="form-control" readonly required></td>
                                <td><input name="price[]" type="text" id="ProductPrice1"
                                           class="form-control ProductEdit" required></td>
                                <td><input id="ProductQuantity1" name="quantity[]" type="number"
                                           class="form-control ProductEdit" min="1" value="1" required>
                                </td>
                                <td><input id="ProductTotal1" type="text" name="prototal[]" class="form-control"
                                           readonly required></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="row mt-5 m-0 p-3">
                            <div class="col-6">
                                <div class="row">
                                    <button type="button" class="btn btn-success AddProduct float-left">Add New
                                        Product
                                    </button>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row mt-2">
                                    <div class="col-6 text-right">
                                        Total:
                                    </div>
                                    <div class="col-6">
                                        <input id="PurchaseTotal" name="total" type="text" class="form-control"
                                               readonly>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-6 text-right">
                                    </div>
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-primary float-left px-4 mr-2">Save</button>
                                        <button type="button"
                                                onclick="window.location.href='{{route('inventory.index')}}'"
                                                class="btn btn-secondary float-left px-4 RemoveNewPurchaseRequisition">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script>
        $('.datepicker').datepicker("setDate", new Date());

        $(document).on('click', '.AddProduct', function () {
            let count = parseInt($(".AddPurchaseDiv tr .Sl").last().text()) + 1;
            let product = '<tr class="AddPurchaseTr' + count + '">\n' +
                '                                        <td class="Sl">' + count + '</td>\n' +
                '                                        <td><input type="text" id="SelectProduct' + count + '" name="product_code[]" class="form-control SelectProduct" required></td>\n' +
                '                                        <td><input name="product_name[]" type="text"\n' +
                '                                           class="form-control" id="product_name' + count + '" readonly></td>\n' +
                '                                        <td><input name="price[]" id="ProductPrice' + count + '" type="text" class="form-control ProductEdit" required></td>\n' +
                '                                        <td><input name="quantity[]" id="ProductQuantity' + count + '" type="number" class="form-control ProductEdit" min="1" value="1" required></td>\n' +
                '                                        <td><input name="prototal[]" id="ProductTotal' + count + '" type="text" class="form-control" readonly required></td>\n' +
                '                                        <td><button type="button" class="btn btn-danger shadow-none px-2" onclick="AddPurchaseDivRemove(\'AddPurchaseTr' + count + '\')" title="Remove Input"><i class="fas fa-1x fa-minus-circle"></i></button></td>\n' +
                '                                    </tr>';
            $(".AddPurchaseDiv").append(product);
        });

        function AddPurchaseDivRemove(data) {
            $('.' + data).remove();
            calculation();
        }

        $(document).on('keyup', '.SelectProduct', function () {
            let productId = $(this).val(), id = $(this).attr('id'), result = false;
            $('.SelectProduct').each(function (i, obj) {
                if ($(this).val() === productId && id !== $(this).attr('id')) result = true;
            });
            if (result) return false;

            id = remove_character('SelectProduct', id);
            if (productId !== '') {
                $.ajax({
                    url: "{!! route('product.show','') !!}" + "/" + productId,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            $('#product_name' + id).val(data.product_name);
                            $('#ProductPrice' + id).val(data.product_unit_price);
                        } else {
                            $('#product_name' + id).val('');
                            $('#ProductPrice' + id).val('');
                        }
                        calculation();
                    }
                });
            }
        });

        $(document).on('keyup', '.ProductEdit', function () {
            if (/\D/g.test(this.value))
                this.value = this.value.replace(/\D/g, '');
            calculation();
        });

        function calculation() {
            let AllClass = $(".AddPurchaseDiv tr .Sl"), subtotal = 0;
            for (let i = 0; i < AllClass.length; i++) {
                let id = parseInt($(AllClass[i]).text());
                productId = $('#SelectProduct' + id).val();
                if (productId !== '') {
                    let price = $("#ProductPrice" + id).val() * $("#ProductQuantity" + id).val();
                    subtotal += Math.trunc(price);
                    $("#ProductTotal" + id).val(Math.trunc(price));
                }
            }
            $('#PurchaseTotal').val(subtotal);
        }

        function remove_character(str_to_remove, str) {
            let reg = new RegExp(str_to_remove)
            return str.replace(reg, '')
        }

        $("input[name=member_edit]").on("click", function () {
            let data = $(this).val();
            if (data == "1") {
                $("#member_id").hide();
                $("#member_id2").show();
            } else {
                $("#member_id").show();
                $("#member_id2").hide();
            }
        });

    </script>

@endpush

@extends('layout.app')
@section('title','Army Golf Club | Supplier Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times-circle mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Stock Manage</span>
                <h3 class="page-title">Stock Delivery</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a id="add-new-event" role="button" href="#" class="btn btn-primary" data-toggle="modal"
                       data-target="#exampleModal">
                        <i class="material-icons">add</i> New Stock Delivery </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->

        @if(isset($edit))
            <div class="row">
                <div class="col-sm-12 mb-4">
                    <!-- Quick Post -->
                    <div class="card card-small h-100">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Update Product Information</h6>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <form class="quick-post-form" method="post"
                                  action="{{route('product.update',$edit->id)}}">
                                @csrf
                                @method('PUT')
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="product_name"
                                               value="{{$edit->product_name}}"
                                               placeholder="Enter Product Name" max="191" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" name="product_code"
                                               value="{{$edit->product_code}}"
                                               placeholder="Enter Product Code" max="191" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="input-group input-group-seamless">
                                            <input type="number" class="form-control" name="product_unit_price"
                                                   value="{{$edit->product_unit_price}}"
                                                   placeholder="Product Per Unit Price" required>
                                            <span class="input-group-append">
                              <span class="input-group-text" style="font-size: 22px">
                                &#x9f3;
                              </span>
                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-accent">Update</button>
                                    <a href="{{route('product.index')}}" role="button" class="btn btn-success mx-2">Close</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Quick Post -->
                </div>
            </div>
        @endif

        <table class="transaction-history d-none">
            <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Department</th>
                <th>Qty</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>@php $sl = 1; @endphp
            @foreach($product as $products)
                <tr>
                    <td>{{$sl}}</td>@php $sl++; @endphp
                    <td>{{$products->product->product_name}}</td>
                    <td>{{$products->product->product_code}}</td>
                    <td>{{$products->department->department_name}}</td>
                    <td>{{$products->qty}}</td>
                    <td>{{date('M dS Y', strtotime($products->date))}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- End Transaction History Table -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('stock.store')}}" autocomplete="off">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Stock Delivery Entry</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-row">
                            <div class="input-daterange input-group input-group-sm ml-auto col-md-6">
                                <input type="text" class="input-sm form-control datepicker" name="date"
                                       placeholder="Date" id="analytics-overview-date-range-1">
                                <span class="input-group-append">
                                    <span class="input-group-text">
                                      <i class="material-icons">&#xE916;</i>
                                    </span>
                                  </span>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control product" name="product_code"
                                       placeholder="Enter Product Code">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control product_name"
                                       placeholder="Product Name" readonly>
                                <input type="hidden" class="product_id" name="product_id" value="">
                            </div>
                            <div class="form-group col-md-6">
                                <select name="department_id" class="form-control department" required>
                                    <option selected disabled>Select Departments</option>
                                    @foreach($department as $departments)
                                        <option value="{{$departments->id}}">{{$departments->department_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="number" class="form-control purchase_unit_price" name="purchase_unit_price"
                                       placeholder="Enter Purchase Unit Price" required readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" class="form-control product-qty-input" name="qty"
                                       placeholder="Enter Product Qty" readonly required>
                                <div class="invalid-feedback product-qty">ok</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script>
        $('.delete').click(function (e) {
            e.preventDefault();
            let linkURL = $(this).attr("href");
            swal({
                title: "Sure want to delete?",
                text: "If you click 'OK' file will be deleted",
                type: "warning",
                showCancelButton: true
            }, function () {
                window.location.href = linkURL;
            });
        });

        $('.edit').click(function (e) {
            let linkURL = $(this).attr("href");
            window.location.href = linkURL;
        });

        $('.datepicker').datepicker("setDate", new Date());

        $(document).ready(function () {
            $(document).on('keyup', '.product', function () {
                let code = $(this).val();
                $.ajax({
                    url: "{!! route('product.show','') !!}" + "/" + code,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            $('.product_name').val(data.product_name);
                            $('.product_id').val(data.id);
                            $('.purchase_unit_price').val(data.product_unit_price);
                            quantity();
                        } else {
                            $('.product_name').val('');
                            $('.product_id').val('');
                            $('.purchase_unit_price').val('');
                        }
                    }
                });
            });
        });

        function quantity() {
            let product = $('.product_id').val(), id = $('.product').val();
            if (product != "") {
                $.ajax({
                    url: "{{route('inventory.qty')}}",
                    type: 'get',
                    data: {product: id},
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            $('.product-qty').show();
                            $('.product-qty').html(data + ' Product available');
                            $('.product-qty-input').prop('readonly', false);
                        } else {
                            $('.product-qty').show();
                            $('.product-qty').html('No product found');
                            $('.product-qty-input').prop('readonly', true).val('');
                        }
                    }
                });
            }
        }
    </script>
@endpush

@extends('layout.app')
@section('title','Army Golf Club | Report Supplier')
@section('content')
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Report</span>
                <h3 class="page-title">Report Supplier</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div id="printbar" style="float:right;margin-top: 7px;margin-right: 7px"></div>
        <table class="transaction d-none">
            <thead>
            <tr>
                <th>Supplier Id</th>
                <th>Company Name</th>
                <th>Owner Name</th>
                <th>Cell Number</th>
                <th>Email</th>
                <th>Address</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product as $products)
                <tr>
                    <td>{{$products->supplier_id}}</td>
                    <td>{{$products->company_name}}</td>
                    <td>{{$products->owner_name}}</td>
                    <td>{{$products->cell_number}}</td>
                    <td>{{$products->email}}</td>
                    <td>{{$products->address}}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function () {
            var table = $('.transaction').DataTable({
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fas fa-copy mr-1"></i> Copy',
                        titleAttr: 'Copy'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel mr-1"></i> Excel',
                        titleAttr: 'Excel'
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fas fa-file-csv mr-1"></i> CSV',
                        titleAttr: 'CSV'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf mr-1"></i> PDF',
                        titleAttr: 'PDF'
                    }

                ]
            });

            table.buttons().container().appendTo($('#printbar'));
            $('.dt-buttons').addClass('btn-group d-table ml-auto');
            $('.dt-buttons button').addClass('btn btn-sm btn-white');

        });
    </script>

@endpush

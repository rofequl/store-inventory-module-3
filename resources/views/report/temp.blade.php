@extends('layout.app')
@section('title','Army Golf Club | Daily Temp. Out / In Ledger')
@section('content')
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Report</span>
                <h3 class="page-title">Daily Temp. Out / In Ledger</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
                <div class="form-row">
                    <div class="input-daterange input-group input-group-sm col-md-3">
                        <input type="text" class="input-sm form-control datepicker" value="{{$start_date}}" name="date"
                               autocomplete="off"
                               placeholder="Start Date" id="analytics-overview-date-range-1" required>
                        <span class="input-group-append">
                                            <span class="input-group-text">
                                              <i class="material-icons">&#xE916;</i>
                                            </span>
                                          </span>
                    </div>
                    <div class="input-daterange input-group input-group-sm col-md-3">
                        <input type="text" class="input-sm form-control datepicker" value="{{$end_date}}" name="date2"
                               autocomplete="off"
                               placeholder="End Date" id="analytics-overview-date-range-1" required>
                        <span class="input-group-append">
                                            <span class="input-group-text">
                                              <i class="material-icons">&#xE916;</i>
                                            </span>
                                          </span>
                    </div>
                    <div class="input-daterange input-group-sm col-md-3">
                        <select class="input-sm form-control product_id"
                                onchange="if (this.selectedIndex) product();">
                            <option selected disabled>Select Product</option>
                            @foreach($product_name as $products)
                                <option
                                    value="{{$products->product_code}}" {{$product_id == $products->product_code?'selected':''}}>{{$products->product_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-daterange input-group-sm col-md-2">
                        <a href="{{route('temp.report')}}" class="btn btn-sm btn-accent">Reset Search</a>
                    </div>
                </div>
            </div>
            <div class="card-body p-0 pb-0 text-center">
                <div id="printbar" style="float:right;margin-top: 7px;margin-right: 7px"></div>
                <table class="transaction d-none table mb-0">
                    <thead class="bg-light">
                    <tr>
                        <th scope="col" class="border-0">#</th>
                        <th scope="col" class="border-0">Out Date</th>
                        <th scope="col" class="border-0">In Date</th>
                        <th scope="col" class="border-0">Department</th>
                        <th scope="col" class="border-0">Receiver Name</th>
                        <th scope="col" class="border-0">Product Name</th>
                        <th scope="col" class="border-0">Product Code</th>
                        <th scope="col" class="border-0">Qty</th>
                        <th scope="col" class="border-0">Out</th>
                        <th scope="col" class="border-0">In</th>
                    </tr>
                    </thead>
                    <tbody>@php $sl = 1; @endphp
                    @foreach($product as $products)
                        <tr>
                            <td>{{$sl}}</td>@php $sl++; @endphp
                            <td>{{date('M dS Y', strtotime($products->date))}}</td>
                            <td>{{date('M dS Y', strtotime($products->updated_at))}}</td>
                            <td>{{$products->department->department_name}}</td>
                            <td>{{$products->receiver_name}}</td>
                            <td>{{$products->product->product_name}}</td>
                            <td>{{$products->product->product_code}}</td>
                            <td>{{$products->qty}}</td>
                            <td>{{$products->out}}</td>
                            <td>{{$products->in}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({autoclose: true, format: 'dd/mm/yyyy'}).on('changeDate', function (e) {
                if (e.target.name == 'date') {
                    var url = window.location.href;
                    url = new URL(url);
                    if (url.searchParams.get("start_date")) {
                        url.searchParams.set('start_date', e.format(0, "dd/mm/yyyy"));
                        window.location.replace(url.href);
                    } else {
                        url.searchParams.append('start_date', e.format(0, "dd/mm/yyyy"));
                        window.location.replace(url.href);
                    }
                }

                if (e.target.name == 'date2') {
                    var url = window.location.href;
                    url = new URL(url);
                    if (url.searchParams.get("end_date")) {
                        url.searchParams.set('end_date', e.format(0, "dd/mm/yyyy"));
                        window.location.replace(url.href);
                    } else {
                        url.searchParams.append('end_date', e.format(0, "dd/mm/yyyy"));
                        window.location.replace(url.href);
                    }
                }
                //console.log(e.format(0,"yyyy/mm/dd"));
            });

            var table = $('.transaction').DataTable({
                buttons: [
                    {
                        extend: 'copyHtml5',
                        text: '<i class="fas fa-copy mr-1"></i> Copy',
                        titleAttr: 'Copy'
                    },
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fas fa-file-excel mr-1"></i> Excel',
                        titleAttr: 'Excel'
                    },
                    {
                        extend: 'csvHtml5',
                        text: '<i class="fas fa-file-csv mr-1"></i> CSV',
                        titleAttr: 'CSV'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fas fa-file-pdf mr-1"></i> PDF',
                        titleAttr: 'PDF'
                    }

                ]
            });

            table.buttons().container().appendTo($('#printbar'));
            $('.dt-buttons').addClass('btn-group d-table ml-auto');
            $('.dt-buttons button').addClass('btn btn-sm btn-white');

        });

        function product() {
            var url = window.location.href;
            var product_id = $('.product_id').val();
            url = new URL(url);
            if (url.searchParams.get("product")) {
                url.searchParams.set('product', product_id);
                window.location.replace(url.href);
            } else {
                url.searchParams.append('product', product_id);
                window.location.replace(url.href);
            }
        }
    </script>

@endpush

@extends('layout.app')
@section('title','Army Golf Club | Supplier Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times-circle mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Product Management</span>
                <h3 class="page-title">Stock Entry</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a id="add-new-event" href="{{route('inventory.add')}}" class="btn btn-primary">
                        <i class="material-icons">add</i> New Stock Entry</a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->

        <table class="transaction-history d-none">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Voucher No.</th>
                <th>Supplier Name / Source</th>
                <th>Total Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>@php $sl = 1; @endphp
            @foreach($inventory as $inventories)
                <tr>
                    <td>{{$sl}}</td>@php $sl++; @endphp
                    <td>{{date('M dS Y', strtotime($inventories->date))}}</td>
                    <td>{{$inventories->purchase_code}}</td>
                    <td>{{$inventories->source_id==Null?$inventories->supplier->owner_name:$inventories->source->name}}</td>
                    <td>{{$inventories->total}}</td>
                    <td>
                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                            <button type="button" class="btn btn-white view" id="{{$inventories->id}}">
                                <i class="material-icons">
                                    remove_red_eye
                                </i>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- End Transaction History Table -->
    </div>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div>
                        <img id="main-logo" class="d-block mx-auto" style="max-width: 50px;"
                             src="{{asset('assets/images/logo.jpg')}}" alt="Store Inventory">
                        <h4 class="text-center mb-0">Store Inventory</h4>
                        <p class="text-center">Army Golf Club, Dhaka Cantonment</p>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <div class="row">
                                <div class="col-5 text-left">Supplier / Source:</div>
                                <div class="col-md-4">
                                    <span class="supplier_name"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="row justify-content-end">
                                <div class="col-6 text-right">Purchase No:</div>
                                <div class="col-4 text-right">
                                    <span class="purchase_no"></span>
                                </div>
                            </div>
                            <div class="row mt-2 justify-content-end">
                                <div class="col-6 text-right">Current Date:</div>
                                <div class="col-4 text-right">
                                    <span class="current_date"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered mb-0 mt-4 text-center">
                        <thead class="bg-light">
                        <tr>
                            <th scope="col">Product Code</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total Amount</th>
                        </tr>
                        </thead>
                        <tbody class="AddPurchaseDiv">

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script>
        $('.view').click(function () {
            let id = $(this).attr("id");
            $.ajax({
                url: "{!! route('inventory.show','') !!}" + "/" + id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if(data.source_id == null){
                        $('.supplier_name').html(data.supplier.owner_name);
                    }else{
                        $('.supplier_name').html(data.source.name);
                    }
                    $('.purchase_no').html(data.purchase_code);
                    $('.current_date').html(data.date);
                    $(".AddPurchaseDiv").html('');
                    for (let i = 0; i < data.inventory_product.length; i++) {
                        let product = '<tr>\n' +
                            '                                        <td>' + data.inventory_product[i].product_code + '</td>\n' +
                            '                                        <td>' + data.inventory_product[i].product_name + '</td>\n' +
                            '                                        <td>' + data.inventory_product[i].price + '</td>\n' +
                            '                                        <td>' + data.inventory_product[i].quantity + '</td>\n' +
                            '                                        <td>' + data.inventory_product[i].prototal + '</td>\n' +
                            '                                    </tr>';
                        $(".AddPurchaseDiv").append(product);
                    }
                    $('#exampleModal').modal('show');
                }
            });
        });
    </script>
@endpush

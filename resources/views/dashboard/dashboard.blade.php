@extends('dashboard.layout.app')
@section('title','Army Golf Club | Store Room Management')
@section('content')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Army Golf Club</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            @if(user_has_permission(Auth::user()->id, 1))
                <div class="col-lg-2dot4 col-md-6 col-sm-6 mb-4"
                     onclick="window.location.href='https://guest.kajerkhojay.com/'">
                    <div class="stats-small stats-small--1 card card-small">
                        <div class="card-body p-0 d-flex">
                            <div class="chartjs-size-monitor"
                                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto">
                                <div class="stats-small__data text-center">
                                    <h6 class="stats-small__value count my-3">
                                        <i class="fa fa-bed" aria-hidden="true" style="font-size: 50px"></i>
                                    </h6>
                                    <span class="stats-small__label text-uppercase"> Guest Room Man.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>@endif
            @if(user_has_permission(Auth::user()->id, 2))
                <div class="col-lg-2dot4 col-md-6 col-sm-6 mb-4" onclick="window.location.href='{{route('store')}}'">
                    <div class="stats-small stats-small--1 card card-small">
                        <div class="card-body p-0 d-flex">
                            <div class="chartjs-size-monitor"
                                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto">
                                <div class="stats-small__data text-center">
                                    <h6 class="stats-small__value count my-3">
                                        <i class="fa fa-houzz" aria-hidden="true" style="font-size: 50px"></i>
                                    </h6>
                                    <span class="stats-small__label text-uppercase">Store Inventory</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>@endif
            @if(user_has_permission(Auth::user()->id, 3))
                <div class="col-lg-2dot4 col-md-6 col-sm-6 mb-4">
                    <div class="stats-small stats-small--1 card card-small">
                        <div class="card-body p-0 d-flex">
                            <div class="chartjs-size-monitor"
                                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto">
                                <div class="stats-small__data text-center">
                                    <h6 class="stats-small__value count my-3">
                                        <i class="fa fa-coffee" aria-hidden="true" style="font-size: 50px"></i>
                                    </h6>
                                    <span class="stats-small__label text-uppercase">Golf Cafe Man.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>@endif
            @if(user_has_permission(Auth::user()->id, 5))
                <div class="col-lg-2dot4 col-md-6 col-sm-6 mb-4"
                     onclick="window.location.href='http://kajerkhojay.com/golf_garden/'">
                    <div class="stats-small stats-small--1 card card-small">
                        <div class="card-body p-0 d-flex">
                            <div class="chartjs-size-monitor"
                                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto">
                                <div class="stats-small__data text-center">
                                    <h6 class="stats-small__value count my-3">
                                        <i class="fa fa-tree" aria-hidden="true" style="font-size: 50px"></i>
                                    </h6>
                                    <span class="stats-small__label text-uppercase">Golf Garden Man.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>@endif
            @if(user_has_permission(Auth::user()->id, 6))
                <div class="col-lg-2dot4 col-md-6 col-sm-6 mb-4"
                     onclick="window.location.href='http://account.kajerkhojay.com/'">
                    <div class="stats-small stats-small--1 card card-small">
                        <div class="card-body p-0 d-flex">
                            <div class="chartjs-size-monitor"
                                 style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                     style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <div class="d-flex flex-column m-auto">
                                <div class="stats-small__data text-center">
                                    <h6 class="stats-small__value count my-3">
                                        <i class="fa fa-money" aria-hidden="true" style="font-size: 50px"></i>
                                    </h6>
                                    <span class="stats-small__label text-uppercase">Central Accounts</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>@endif
        </div>

    </div>
@endsection
@push('style')
    <style>
        .col-lg-2dot4 {
            position: relative;
            width: 100%;
            min-height: 1px;
            padding-right: 15px;
            padding-left: 15px;
            cursor: pointer;
        }

        @media (min-width: 960px) {
            .col-lg-2dot4 {
                -webkit-box-flex: 0;
                -ms-flex: 0 0 20%;
                flex: 0 0 20%;
                max-width: 20%;
            }
        }
    </style>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/app/app-ecommerce.1.3.1.min.js')}}"></script>
@endpush


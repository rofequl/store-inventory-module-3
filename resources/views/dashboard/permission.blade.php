@extends('dashboard.layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Role Permission</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">

            </div>
        </div>
        <!-- End Page Header -->

        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-body p-0 pb-3 text-left">
                        <div class="table-responsive">
                            <table class="table mb-0" style="font-size: 10px">
                                <thead>
                                <tr>
                                    <th>Role Name</th>
                                    @foreach($permission as $permissions)
                                        <th>{{$permissions->name}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($role as $roles)
                                    <tr>
                                        <td>{{$roles->name}}</td>
                                        @foreach($permission as $permissions)
                                            <td>
                                                <div class="custom-control custom-checkbox mb-1">
                                                    <input data-roleID="{{$roles->id}}"
                                                           data-permissionId="{{$permissions->id}}" type="checkbox"
                                                           class="custom-control-input percheck"
                                                           id="{{$roles->id}}formsCheckboxDefault{{$permissions->id}}"
                                                        {{role_has_permission($roles->id, $permissions->id)?'checked':''}}>
                                                    <label class="custom-control-label"
                                                           for="{{$roles->id}}formsCheckboxDefault{{$permissions->id}}"></label>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script>
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $('.percheck').change(function () {
            let roleId = $(this).attr("data-roleID"), permissionId = $(this).attr("data-permissionId"), action;
            $(this).prop('checked') ? action = 'add' : action = 'remove';
            $.ajax({
                url: '{{route('RolePermissionStore')}}',
                type: 'POST',
                data: {_token: CSRF_TOKEN, roleId: roleId, permissionId: permissionId, action: action},
                dataType: 'JSON',
                success: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
@endpush

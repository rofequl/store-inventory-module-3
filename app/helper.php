<?php

use App\condition_product;
use App\product_status;
use App\role_has_permission;
use App\user;

function product_condition($product_id, $condition_id)
{
    return condition_product::where('product_id', $product_id)->where('condition_id', $condition_id)->get()->sum('qty');
}

function role_has_permission($roleId, $permissionId)
{
    return role_has_permission::where('role_id', $roleId)->where('permission_id', $permissionId)->first();
}

function user_has_permission($userId, $permissionId)
{
    $user = user::findOrFail($userId);
    return role_has_permission::where('role_id', $user->role_id)->where('permission_id', $permissionId)->first();
}

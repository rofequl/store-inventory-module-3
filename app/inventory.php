<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventory extends Model
{

    public function supplier()
    {
        return $this->belongsTo(supplier::class);
    }

    public function source()
    {
        return $this->belongsTo(source::class);
    }

    public function inventory_product(){
        return $this->hasMany(inventory_product::class,'inventory_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplier extends Model
{
    public function inventory(){
        return $this->hasMany(inventory::class,'supplier_id');
    }
}

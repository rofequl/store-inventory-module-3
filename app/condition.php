<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class condition extends Model
{
    public function condition_product(){
        return $this->hasMany(condition_product::class,'condition_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temp extends Model
{
    public function department()
    {
        return $this->belongsTo(department::class);
    }

    public function product()
    {
        return $this->belongsTo(product::class, 'product_id', 'product_code');
    }
}

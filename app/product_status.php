<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_status extends Model
{
    public function product()
    {
        return $this->belongsTo(product::class,'product_id','product_code');
    }

    public function department()
    {
        return $this->belongsTo(department::class);
    }

}

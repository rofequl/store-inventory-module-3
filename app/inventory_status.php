<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventory_status extends Model
{
    public function product()
    {
        return $this->belongsTo(product::class, 'product_id', 'product_code');
    }
}

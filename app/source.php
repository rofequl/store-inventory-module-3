<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class source extends Model
{
    public function inventory(){
        return $this->hasMany(inventory::class,'source_id');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\login_record;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    protected function create(Request $request)
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'role_id' => 'required|string',
        ]);

        user::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'role_id' => $request['role_id'],
            'password' => Hash::make($request['password']),
        ]);

        Session::flash('message', 'User register successfully');
        return redirect('user-manage');
    }

    protected function ProfileUpdate(Request $request, $id)
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users,email,' . base64_decode($id),
            'role_id' => 'required|string',
            'phone' => 'max:26',
            'address' => 'max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        $user = user::findOrFail(base64_decode($id));
        if ($request->hasFile('image')) {
            if (File::exists($user->image) && 'assets/images/user.png' != $user->image) {
                File::delete($user->image);
            }
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension;
            $request->file('image')->storeAs('public/user', $fileStore3);
            $image = 'storage/user/' . $fileStore3;
        } else {
            $image = $user->image;
        }

        user::findOrFail(base64_decode($id))->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'image' => $image,
            'role_id' => $request['role_id'],
        ]);

        Session::flash('message', 'User profile update successfully');
        return redirect()->back();
    }

    protected function ProfilePasswordChange(Request $request, $id)
    {
        if (!user_has_permission(Auth::user()->id, 4)) abort(404);
        $request->validate([
            'password' => 'required|string|min:8|confirmed',
        ]);

        user::findOrFail(base64_decode($id))->update([
            'password' => Hash::make($request['password']),
        ]);

        Session::flash('message', 'User password change');
        return redirect()->back();
    }

    protected function delete($id)
    {
        $login = login_record::where('user_id', base64_decode($id))->get();
        foreach ($login as $logins) {
            login_record::findOrFail($logins->id)->delete();
        }

        user::findOrFail(base64_decode($id))->delete();

        Session::flash('message', 'User delete successfully');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\login_record;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function showUserLoginForm()
    {
        return view('auth.login');
    }

    public function userLogin(Request $request)
    {
        $messages = [
            "email.required" => "Please enter your email address",
            "password.required" => "Please enter your password",
            "password.min" => "Password must be at least 6 characters"
        ];
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ], $messages);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            $ip = \Request::ip();
            $browser = $_SERVER['HTTP_USER_AGENT'];

            $check = login_record::where('ip_address', $ip)->where('browser',$browser)->first();
            if (!$check) {
                $insert = new login_record();
                $insert->ip_address = $ip;
                $insert->browser = $browser;
                $insert->user_id = Auth::user()->id;
                $insert->save();
            } else {
                $check->user_id = Auth::user()->id;
                $check->save();
            }
            return redirect()->intended('/');
        }
        return back()->withInput($request->only('phone', 'remember'))->withErrors([
            'email' => 'Wrong information or this account not login.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $ip = \Request::ip();
        $browser = $_SERVER['HTTP_USER_AGENT'];

        $check = login_record::where('ip_address', $ip)->where('browser',$browser)->first();
        if ($check) {
            $check->delete();
        }

        return redirect('/');
    }


}

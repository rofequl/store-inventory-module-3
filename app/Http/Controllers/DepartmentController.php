<?php

namespace App\Http\Controllers;

use App\condition_product;
use App\departement_poduct;
use App\department;
use App\inventory;
use App\product_status;
use App\temp;
use Illuminate\Http\Request;
use Session;

class DepartmentController extends Controller
{
    public function index()
    {
        $department = department::orderBy('id', 'DESC')->get();
        return view('department', compact('department'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'department_name' => 'required|max:191|unique:departments,department_name',
        ]);

        $insert = new department();
        $insert->department_name = $request->department_name;
        $insert->save();

        Session::flash('message', 'Department add successfully');
        return redirect('department');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $edit = department::findOrFail($id);
        $department = department::orderBy('id', 'DESC')->get();
        return view('department', compact('department', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'department_name' => 'required|max:191|unique:departments,department_name,' . $id,
        ]);

        $insert = department::findOrFail($id);
        $insert->department_name = $request->department_name;
        $insert->save();

        Session::flash('message', 'Department update successfully');
        return redirect('department');
    }

    public function destroy($id)
    {
        $product_status = product_status::where('department_id', $id)->get();
        $condition_product = condition_product::where('department_id', $id)->get();
        $departement_poduct = departement_poduct::where('department_id', $id)->get();
        $temp = temp::where('department_id', $id)->get();

        if ($condition_product->count() > 0 || $product_status->count() > 0 || $departement_poduct->count() > 0 || $temp->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Department already use, you can\'t delete']]);
        }

        $department = department::findOrFail($id);
        $department->delete();

        Session::flash('message', 'Department delete successfully');
        return redirect('department');
    }
}

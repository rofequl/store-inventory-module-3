<?php

namespace App\Http\Controllers;

use App\condition;
use App\condition_product;
use App\product_status;
use Illuminate\Http\Request;
use Session;

class ConditionController extends Controller
{
    public function index()
    {
        $condition = condition::orderBy('id', 'DESC')->get();
        return view('condition', compact('condition'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'working_condition' => 'required|max:191|unique:conditions,working_condition',
        ]);

        $insert = new condition();
        $insert->working_condition = $request->working_condition;
        $insert->save();

        Session::flash('message', 'Working condition add successfully');
        return redirect('condition');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = condition::findOrFail($id);
        $condition = condition::orderBy('id', 'DESC')->get();
        return view('condition', compact('condition', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'working_condition' => 'required|max:191|unique:conditions,working_condition,' . $id,
        ]);

        $insert = condition::findOrFail($id);
        $insert->working_condition = $request->working_condition;
        $insert->save();

        Session::flash('message', 'Working condition update successfully');
        return redirect('condition');
    }

    public function destroy($id)
    {
        $product_status = condition_product::where('condition_id', $id)->get();
        if ($product_status->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Condition already use, you can\'t delete']]);
        }
        $condition = condition::findOrFail($id);
        $condition->delete();

        Session::flash('message', 'Working Condition delete successfully');
        return redirect('condition');
    }
}

<?php

namespace App\Http\Controllers;

use App\departement_poduct;
use App\department;
use App\inventory_status;
use App\product;
use App\product_status;
use DateTime;
use Illuminate\Http\Request;
use Session;

class StockController extends Controller
{
    public function index()
    {
        $department = department::latest()->get();
        $product = departement_poduct::orderBy('id', 'DESC')->get();
        return view('stock', compact('product', 'department'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|max:191',
            'product_id' => 'required',
            'product_code' => 'required',
            'purchase_unit_price' => 'required',
            'qty' => 'required|numeric',
            'department_id' => 'required|numeric',
        ]);

        $inventory = inventory_status::where('product_id', $request->product_code)->first();
        if (!$inventory || $inventory->qty < $request->qty) {
            return redirect('stock')->withErrors(['You has not much product']);
        }

        $date = DateTime::createFromFormat('m/d/Y', $request->date);
        $insert = new departement_poduct();
        $insert->date = $date->format('Y-m-d');
        $insert->product_id = $request->product_code;
        $insert->qty = $request->qty;
        $insert->department_id = $request->department_id;
        $insert->save();

        if ($inventory) {
            $qty = $inventory->qty - $request->qty;
            $inventory->qty = $qty;
            $inventory->save();
        }

        $inventory = product_status::where('product_id', $request->product_code)->where('department_id', $request->department_id)->first();
        if ($inventory) {
            $qty = $inventory->qty + $request->qty;
            $inventory->qty = $qty;
            $inventory->save();
        } else {
            $insert = new product_status();
            $insert->product_id = $request->product_code;
            $insert->qty = $request->qty;
            $insert->department_id = $request->department_id;
            $insert->save();
        }

        Session::flash('message', 'Stock delivery entry successfully');
        return redirect('stock');
    }

    public function qty(Request $request)
    {
        $inventory = product_status::where('product_id', $request->product)->where('department_id', $request->department)->first();
        if ($inventory) {
            return $inventory->qty;
        } else {
            return 0;
        }
    }
}

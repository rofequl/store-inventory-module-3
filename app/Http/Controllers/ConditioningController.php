<?php

namespace App\Http\Controllers;

use App\condition_product;
use App\inventory;
use App\inventory_status;
use DateTime;
use Session;
use App\condition;
use App\department;
use App\product_status;
use Illuminate\Http\Request;

class ConditioningController extends Controller
{
    public function index()
    {
        $condition = condition::latest()->get();
        $department = department::latest()->get();
        $inventory = condition_product::orderBy('id', 'DESC')->get();
        return view('conditioning', compact('inventory', 'department', 'condition'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|max:191',
            'product_id' => 'required',
            'product_code' => 'required',
            'qty' => 'required|numeric',
            'condition_id' => 'required|numeric',
        ]);

        if ($request->department_id == '') {
            $inventory = inventory_status::where('product_id', $request->product_code)->first();
            if (!$inventory) return redirect('conditioning')->withErrors(['Something wrong please try again later.']);
        } else {
            $inventory = product_status::where('product_id', $request->product_code)->where('department_id', $request->department_id)->first();
            if (!$inventory) return redirect('conditioning')->withErrors(['Something wrong please try again later.']);
        }

        if (!$inventory || $inventory->qty < $request->qty) {
            Session::flash('message', 'You has not much product');
            return redirect('conditioning');
        }

        $date = DateTime::createFromFormat('m/d/Y', $request->date);
        $insert = new condition_product();
        $insert->date = $date->format('Y-m-d');
        $insert->product_id = $request->product_code;
        $insert->qty = $request->qty;
        $insert->department_id = $request->department_id;
        $insert->condition_id = $request->condition_id;
        $insert->remarks = $request->remarks;
        $insert->save();


        if ($inventory) {
            $qty = $inventory->qty - $request->qty;
            $inventory->qty = $qty;
            $inventory->save();
        }

        Session::flash('message', 'Inventory Conditioning entry successfully');
        return redirect('conditioning');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $product = condition_product::latest()->first();
        if ($product->id == $id) {
            $product_status = condition_product::findOrFail($id);
            if ($product_status->department_id == null) {
                $inventory = inventory_status::where('product_id', $product_status->product_id)->first();
                if (!$inventory) return redirect('conditioning')->withErrors(['You Can\'t delete this inventory']);
                $qty = $inventory->qty + $product_status->qty;
                $inventory->qty = $qty;
                $inventory->save();
            } else {
                $inventory = product_status::where('product_id', $product_status->product_id)->where('department_id', $product_status->department_id)->first();
                if (!$inventory) return redirect('conditioning')->withErrors(['You Can\'t delete this inventory']);
                $qty = $inventory->qty + $product_status->qty;
                $inventory->qty = $qty;
                $inventory->save();
            }

            $product_status->delete();

            Session::flash('message', 'Inventory conditioning delete successfully');
            return redirect('conditioning');
        } else {
            return redirect('conditioning')->withErrors(['You Can\'t delete this inventory']);
        }
    }
}

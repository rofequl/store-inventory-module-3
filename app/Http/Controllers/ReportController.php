<?php

namespace App\Http\Controllers;

use App\condition;
use App\condition_product;
use App\department;
use App\inventory;
use App\inventory_status;
use App\product;
use DateTime;
use App\product_status;
use App\supplier;
use App\temp;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function stock()
    {
        $inventory = inventory_status::latest()->get();
        return view('report.stock', compact('inventory'));
    }

    public function department()
    {
        $inventory = product_status::latest()->get();
        return view('report.department', compact('inventory'));
    }

    public function supplier()
    {
        $product = supplier::all();
        return view('report.supplier', compact('product'));
    }

    public function store(Request $request)
    {
        $start_date = '';
        $end_date = '';
        $condition_id = '';
        $inventory = condition_product::orderBy('id', 'DESC');
        if ($request->start_date != null) {
            $date = DateTime::createFromFormat('d/m/Y', $request->start_date);
            $inventory = $inventory->where('date', '>=', $date->format('Y-m-d'));
            $start_date = $request->start_date;
        }

        if ($request->end_date != null) {
            $date = DateTime::createFromFormat('d/m/Y', $request->end_date);
            $inventory = $inventory->where('date', '<=', $date->format('Y-m-d'));
            $end_date = $request->end_date;
        }

        if ($request->condition != null) {
            $inventory = $inventory->where('condition_id', $request->condition);
            $condition_id = $request->condition;
        }
        $condition = condition::latest()->get();
        $department = department::latest()->get();
        $inventory = $inventory->get();
        return view('report.store', compact('inventory', 'department', 'condition', 'start_date', 'end_date', 'condition_id'));

    }

    public function temp(Request $request)
    {
        //dd($request->all());
        $start_date = '';
        $end_date = '';
        $product_id = '';
        $product = temp::orderBy('id', 'DESC');
        if ($request->start_date != null) {
            $date = DateTime::createFromFormat('d/m/Y', $request->start_date);
            $product = $product->where('date', '>=', $date->format('Y-m-d'));
            $start_date = $request->start_date;
        }

        if ($request->end_date != null) {
            $date = DateTime::createFromFormat('d/m/Y', $request->end_date);
            $product = $product->where('date', '<=', $date->format('Y-m-d'));
            $end_date = $request->end_date;
        }

        if ($request->product != null) {
            $product = $product->where('product_id', $request->product);
            $end_date = $request->end_date;
            $product_id = $request->product;
        }
        $product = $product->get();
        $product_name = product::all();
        //dd($product_name->toArray());
        return view('report.temp', compact('product', 'start_date', 'end_date', 'product_name', 'product_id'));
    }
}

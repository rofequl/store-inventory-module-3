<?php

namespace App\Http\Controllers;

use App\inventory;
use App\inventory_product;
use App\inventory_status;
use App\product;
use App\product_status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function store()
    {
        $supplier = inventory::select('supplier_id', DB::raw('sum(total) as total'))->groupBy('supplier_id')->get();
        $inventory = inventory_status::orderBy('qty', 'DESC')->get()->take(10);
        $product_status = inventory_product::orderBy('id', 'DESC')->get()->take(10);
        return view('index', compact('supplier', 'product_status', 'inventory'));
    }

    public function index()
    {
        return view('dashboard.dashboard');
    }
}

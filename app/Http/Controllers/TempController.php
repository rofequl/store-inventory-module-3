<?php

namespace App\Http\Controllers;

use App\departement_poduct;
use App\department;
use App\inventory_status;
use App\product_status;
use App\temp;
use Session;
use DateTime;
use Illuminate\Http\Request;

class TempController extends Controller
{
    public function index()
    {
        $department = department::latest()->get();
        $product = temp::orderBy('id', 'DESC')->get();
        return view('temp', compact('product', 'department'));
    }

    public function TempOut(Request $request)
    {
        $request->validate([
            'qty' => 'required|numeric',
            'id' => 'required|numeric',
        ]);

        $inventory = temp::findOrFail($request->id);
        if (!$inventory || $inventory->qty < $request->qty) {
            return redirect('temp')->withErrors(['You has not much product in department']);
        }
        $qty = $inventory->qty - $request->qty;
        if ($inventory->qty == null) {
            $inventory->out = $request->qty;
        } else {
            $qty2 = $inventory->out + $request->qty;
            $inventory->out = $qty2;
        }
        $inventory->qty = $qty;
        $inventory->save();

        $insert = inventory_status::where('product_id', $inventory->product_id)->first();
        if ($insert) {
            $qty3 = $insert->qty + $request->qty;
            $insert->qty = $qty3;
            $insert->save();
        }

        Session::flash('message', 'Daily Temp. Out entry successfully');
        return redirect('temp');
    }

    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|max:191',
            'product_id' => 'required',
            'product_code' => 'required',
            'receiver_name' => 'required|max:191',
            'qty' => 'required|numeric',
            'department_id' => 'required|numeric',
        ]);

        $inventory = inventory_status::where('product_id', $request->product_code)->first();
        if (!$inventory || $inventory->qty < $request->qty) {
            return redirect('stock')->withErrors(['You has not much product']);
        }

        $date = DateTime::createFromFormat('m/d/Y', $request->date);
        $insert = new temp();
        $insert->date = $date->format('Y-m-d');
        $insert->product_id = $request->product_code;
        $insert->receiver_name = $request->receiver_name;
        $insert->qty = $request->qty;
        $insert->in = $request->qty;
        $insert->department_id = $request->department_id;
        $insert->save();

        if ($inventory) {
            $qty = $inventory->qty - $request->qty;
            $inventory->qty = $qty;
            $inventory->save();
        }

        Session::flash('message', 'Daily Temp. In entry successfully');
        return redirect('temp');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\inventory;
use App\product;
use App\supplier;
use Illuminate\Http\Request;
use Session;

class SupplierController extends Controller
{

    public function index()
    {
        $supplier = supplier::orderBy('id', 'DESC')->get();
        return view('supplier', compact('supplier'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'owner_name' => 'required|string|max:191',
            'company_name' => 'required|string|max:191',
            'cell_number' => 'required|numeric',
            'address' => 'required|string',
            'email' => 'required|max:191|email',
        ]);
        $id = supplier::latest()->first();
        $id = $id?$id:1;
        $insert = new supplier();
        $insert->supplier_id = rand(100, 999) . '' . $id3;
        $insert->owner_name = $request->owner_name;
        $insert->company_name = $request->company_name;
        $insert->cell_number = $request->cell_number;
        $insert->address = $request->address;
        $insert->email = $request->email;
        $insert->save();

        Session::flash('message', 'Supplier add successfully');
        return redirect('supplier');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = supplier::findOrFail($id);
        $supplier = supplier::orderBy('id','DESC')->get();
        return view('supplier',compact('supplier','edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'owner_name' => 'required|string|max:191',
            'company_name' => 'required|string|max:191',
            'cell_number' => 'required|numeric',
            'address' => 'required|string',
            'email' => 'required|max:191|email',
        ]);

        $insert = supplier::findOrFail($id);
        $insert->owner_name = $request->owner_name;
        $insert->company_name = $request->company_name;
        $insert->cell_number = $request->cell_number;
        $insert->address = $request->address;
        $insert->email = $request->email;
        $insert->save();

        Session::flash('message', 'Supplier update successfully');
        return redirect('supplier');
    }

    public function destroy($id)
    {
        $product = inventory::where('supplier_id', $id)->get();
        if ($product->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Supplier already use, you can\'t delete']]);
        }
        $supplier = supplier::findOrFail($id);
        $supplier->delete();

        Session::flash('message', 'Supplier delete successfully');
        return redirect('supplier');
    }
}

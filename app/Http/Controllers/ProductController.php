<?php

namespace App\Http\Controllers;

use App\inventory_product;
use App\product;
use App\supplier;
use Illuminate\Http\Request;
use Session;

class ProductController extends Controller
{
    public function index()
    {
        $product = product::orderBy('id', 'DESC')->get();
        return view('product', compact('product'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required|string',
            'product_name' => 'required|string|max:191|unique:products,product_name',
            'product_code' => 'required|string|max:191|unique:products,product_code',
            'product_unit_price' => 'required|numeric',
        ]);

        $insert = new product();
        $insert->category = $request->category;
        $insert->product_name = $request->product_name;
        $insert->product_code = $request->product_code;
        $insert->product_unit_price = $request->product_unit_price;
        $insert->save();

        Session::flash('message', 'Product add successfully');
        return redirect('product');
    }

    public function show($id)
    {
        $product = product::where('product_code', $id)->first();
        if ($product) {
            return $product;
        } else {
            return 0;
        }
    }

    public function edit($id)
    {
        $edit = product::findOrFail($id);
        $product = product::orderBy('id', 'DESC')->get();
        return view('product', compact('product', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category' => 'required|string',
            'product_name' => 'required|string|max:191|unique:products,product_name,' . $id,
            'product_code' => 'required|string|max:191|unique:products,product_code,' . $id,
            'product_unit_price' => 'required|numeric',
        ]);

        $insert = product::findOrFail($id);
        $insert->category = $request->category;
        $insert->product_name = $request->product_name;
        $insert->product_code = $request->product_code;
        $insert->product_unit_price = $request->product_unit_price;
        $insert->save();

        Session::flash('message', 'Product update successfully');
        return redirect('product');
    }

    public function destroy($id)
    {
        $supplier = product::findOrFail($id);
        $product = inventory_product::where('product_code', $supplier->product_code)->get();
        if ($product->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Product already use, you can\'t delete']]);
        }
        $supplier->delete();

        Session::flash('message', 'Product delete successfully');
        return redirect('product');
    }
}

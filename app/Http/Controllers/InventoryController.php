<?php

namespace App\Http\Controllers;

use App\inventory_product;
use App\inventory_status;
use App\source;
use App\supplier;
use DateTime;
use PhpParser\Node\Stmt\DeclareDeclare;
use Session;
use App\department;
use App\inventory;
use App\product_status;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function index()
    {
        $department = department::latest()->get();
        $inventory = inventory::orderBy('id', 'DESC')->get();
        return view('inventory', compact('inventory', 'department'));
    }

    public function InventoryAdd()
    {
        $department = department::latest()->get();
        $supplier = supplier::latest()->get();
        $source = source::latest()->get();
        $purchase_no = rand(100, 999) . inventory::all()->count();
        return view('inventory_add', compact('supplier', 'department', 'purchase_no', 'source'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|max:191',
            'purchase_code' => 'required',
            'total' => 'required',
            'product_code.*' => 'required',
            'product_name.*' => 'required',
            'price.*' => 'required',
            'quantity.*' => 'required',
            'prototal.*' => 'required',
        ]);

        if ($request->member_edit == 0) {
            $request->validate([
                'supplier_id' => 'required',
            ]);
        } else {
            $request->validate([
                'source_id' => 'required',
            ]);
        }
        //dd($request->all());

        $date = DateTime::createFromFormat('m/d/Y', $request->date);
        $insert = new inventory();
        $insert->date = $date->format('Y-m-d');
        if ($request->member_edit == 0) $insert->supplier_id = $request->supplier_id;
        else $insert->source_id = $request->source_id;
        $insert->purchase_code = $request->purchase_code;
        $insert->total = $request->total;
        $insert->save();
        $inventory_id = $insert->id;

        for ($i = 0; $i < count($request->product_code); $i++) {
            $insert2 = new inventory_product();
            $insert2->inventory_id = $inventory_id;
            $insert2->product_code = $request->product_code[$i];
            $insert2->product_name = $request->product_name[$i];
            $insert2->price = $request->price[$i];
            $insert2->quantity = $request->quantity[$i];
            $insert2->prototal = $request->prototal[$i];
            $insert2->save();
            $inventory = inventory_status::where('product_id', $request->product_code[$i])->first();
            if ($inventory) {
                $qty = $inventory->qty + $request->quantity[$i];
                $inventory->qty = $qty;
                $inventory->save();
            } else {
                $insert = new inventory_status();
                $insert->product_id = $request->product_code[$i];
                $insert->qty = $request->quantity[$i];
                $insert->save();
            }
        }

        Session::flash('message', 'Inventory entry successfully');
        return redirect('inventory');
    }

    public function show($id)
    {
        return json_encode(inventory::with('inventory_product', 'supplier','source')->findOrFail($id));
    }

    public function edit($id)
    {
        //
    }

    public function qty(Request $request)
    {
        $inventory = inventory_status::where('product_id', $request->product)->first();
        if ($inventory) {
            return $inventory->qty;
        } else {
            return 0;
        }
    }

    public function destroy($id)
    {
        $product = inventory::latest()->first();
        if ($product->id == $id) {
            $product_status = inventory::findOrFail($id);

            $inventory = inventory_status::where('product_id', $product_status->product_id)->first();
            $qty = $inventory->qty - $product_status->qty;
            $inventory->qty = $qty;
            $inventory->save();

            $product_status->delete();

            Session::flash('message', 'Inventory delete successfully');
            return redirect('inventory');
        } else {
            return redirect('inventory')->withErrors(['You Can\'t delete this inventory']);
        }
    }
}

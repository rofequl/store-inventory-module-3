<?php

namespace App\Http\Controllers;

use App\inventory;
use App\product;
use App\source;
use App\supplier;
use Illuminate\Http\Request;
use Session;

class SourceController extends Controller
{
    public function index()
    {
        $supplier = source::orderBy('id', 'DESC')->get();
        return view('source', compact('supplier'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:191|unique:sources,name',
        ]);

        $insert = new source();
        $insert->name = $request->name;
        $insert->save();

        Session::flash('message', 'Source add successfully');
        return redirect('source');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = source::findOrFail($id);
        $supplier = source::orderBy('id', 'DESC')->get();
        return view('source', compact('supplier', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:191|unique:sources,name,' . $id,
        ]);

        $insert = source::findOrFail($id);
        $insert->name = $request->name;
        $insert->save();

        Session::flash('message', 'Source name update successfully');
        return redirect('source');
    }

    public function destroy($id)
    {
        $product = inventory::where('source_id', $id)->get();
        if ($product->count() > 0) {
            return redirect()->back()->withErrors(['message' => ['Source already use, you can\'t delete']]);
        }
        $supplier = source::findOrFail($id);
        $supplier->delete();

        Session::flash('message', 'Source delete successfully');
        return redirect('source');
    }
}

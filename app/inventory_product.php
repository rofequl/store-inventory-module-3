<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventory_product extends Model
{
    public function inventory()
    {
        return $this->belongsTo(inventory::class);
    }
}

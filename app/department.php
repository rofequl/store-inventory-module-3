<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    public function product_status()
    {
        return $this->hasMany(product_status::class, 'department_id');
    }

    public function condition_product()
    {
        return $this->hasMany(condition_product::class, 'department_id');
    }

    public function departement_poduct()
    {
        return $this->hasMany(departement_poduct::class, 'department_id');
    }

    public function temp()
    {
        return $this->hasMany(temp::class, 'department_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{

    public function product_status()
    {
        return $this->hasMany(product_status::class);
    }

    public function inventory_status()
    {
        return $this->hasMany(inventory_status::class);
    }

    public function condition_product()
    {
        return $this->hasMany(condition_product::class);
    }

    public function departement_poduct()
    {
        return $this->hasMany(departement_poduct::class);
    }

    public function temp()
    {
        return $this->hasMany(temp::class);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'CheckUser'], function () {
    Route::get('/store', 'HomeController@store')->name('store');

    Route::resource('department', 'DepartmentController');
    Route::get('department-destroy/{department}', 'DepartmentController@destroy')->name('department.delete');

    Route::resource('condition', 'ConditionController');
    Route::get('condition-destroy/{department}', 'ConditionController@destroy')->name('condition.delete');

    Route::resource('supplier', 'SupplierController');
    Route::get('supplier-destroy/{supplier}', 'SupplierController@destroy')->name('supplier.delete');

    Route::resource('source', 'SourceController');
    Route::get('source-destroy/{supplier}', 'SourceController@destroy')->name('source.delete');

    Route::resource('product', 'ProductController');
    Route::get('product-destroy/{product}', 'ProductController@destroy')->name('product.delete');

    Route::resource('inventory', 'InventoryController');
    Route::get('inventory-destroy/{inventory}', 'InventoryController@destroy')->name('inventory.delete');
    Route::get('inventory-add', 'InventoryController@InventoryAdd')->name('inventory.add');
    Route::get('inventory-qty', 'InventoryController@qty')->name('inventory.qty');

    Route::resource('stock', 'StockController');
    Route::get('stock-qty', 'StockController@qty')->name('stock.qty');

    Route::resource('temp', 'TempController');
    Route::post('temp-out', 'TempController@TempOut')->name('temp.out');

    Route::resource('conditioning', 'ConditioningController');
    Route::get('conditioning-destroy/{conditioning}', 'ConditioningController@destroy')->name('conditioning.delete');

    Route::get('stock-report', 'ReportController@stock')->name('stock.report');
    Route::get('department-report', 'ReportController@department')->name('department.report');
    Route::get('report-supplier', 'ReportController@supplier')->name('supplier.report');
    Route::get('store-report', 'ReportController@store')->name('store.report');
    Route::get('temp-report', 'ReportController@temp')->name('temp.report');

});

Route::get('login', 'Auth\LoginController@showUserLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@UserLogin');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/user-manage', 'UserController@UserManage')->name('UserManage');
    Route::post('/user-register', 'Auth\RegisterController@create')->name('UserRegister');
    Route::get('/user-delete/{user}', 'Auth\RegisterController@delete')->name('UserDelete');

    Route::get('/role', 'UserController@role')->name('role');
    Route::post('/role', 'UserController@RoleStore')->name('role.store');
    Route::get('role/{role}', 'UserController@RoleDestroy')->name('role.delete');
    Route::get('role/{role}/edit', 'UserController@RoleEdit')->name('role.edit');
    Route::put('role/{role}', 'UserController@RoleUpdate')->name('role.update');

    Route::get('/role-permission', 'UserController@RolePermission')->name('role.permission');
    Route::post('/role-permission-store', 'UserController@RolePermissionStore')->name('RolePermissionStore');

    Route::get('/profile-edit/{data}/edit', 'UserController@ProfileEdit')->name('ProfileEdit');
    Route::post('/profile-update/{data}', 'Auth\RegisterController@ProfileUpdate')->name('ProfileUpdate');
    Route::post('/profile-password-change/{data}', 'Auth\RegisterController@ProfilePasswordChange')->name('ProfilePasswordChange');

    Route::get('/profile', 'UserController@profile')->name('profile');
    Route::post('/profile/{data}', 'UserController@ProfileStore')->name('profile.store');
    Route::post('/profile-password/{data}', 'UserController@ProfilePassword')->name('profile.password');


});
